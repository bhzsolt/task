#!/bin/bash
# task :: a simple cli interface time management program sleep program written in bash
# Copyright (C) 2021 Bodoki-Halmen Zsolt
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Bodoki-Halmen Zsolt
#   email:  bodokihalmenzsolt[at]gmail.com

# notes on portability:
# set the first line to the location of your bash executable
# dependencies: psleep (python script, included)
#				date, kill, echo, wait, tee, mv, vim, etc (on windows, these should be in WSL)

# set these variables to some value that is ok on your os
pid_file=~/.log/.TASK_PID #	a file which stores the pid of the running task
log_file=~/.log/task # a log file
diary_file=~/.log/task_diary # a diary of the tasks broken up by days
db_file=~/bin/database/tasks # a file in which the daily tasks are saved

# rewrite this so locker_function locks your screen [and if you would like, also displays a custom message]
locker_function() {
	slock echo TIME IS UP FOR: $name | dmenu -fn "Source Code Pro:size=12" -nb black -nf "#fdf6e3" -sb black -sf "#859900" >/dev/null
}

PID=$([[ -e $pid_file ]] && cat $pid_file)

exec 3>>$diary_file

usage() {
	echo -e "usage: $0 command"
	echo -e "commands:"
	echo -e "\t[list]"
	echo -e "\tstart name [[hh:]mm:]ss"
	echo -e "\tstop"
	echo -e "\tpause"
	echo -e "\tresume"
	echo -e "\tedit"
	echo -e "\tadd"
	echo -e "\tclear"
	echo -e "\tnext"
	echo -e "\tquery [all|item]"
	echo -e "\thelp"
	echo -e 'run $ task next'
	echo -e "\tif there are no tasks for the day,"
	echo -e "\tthis will call 'task add',"
	echo -e "\tand call 'task start <first task name>'"
	[[ "$1" == help ]] && exit 0 || exit 1
}

is_running() {
	[[ -e $pid_file ]] || {
		echo "received $1 with no task running, exiting" >>$log_file; 
		exit 0; 
	}
}

log_function() {
	echo "$(date) :: $1" >>$log_file
}

kill_function() {
	kill -s $1 $PID
}

task_function() {
	name=$1; shift
	echo "$(date) :: start[$name] ${@/ /:}" | tee -a $log_file
	psleep "$@" &
	echo $! >$pid_file
	wait
	rm -rf $pid_file
	echo $(date) :: finished[$name] >>$log_file
	locker_function
}

opt=$1
shift
case "$opt" in
	start)
		[ $# -lt 2 ] && usage
		duration=${2//:/ }
		task_function $1 $duration &
		;;
	pause)
		is_running 'pause'
		log_function pause
		kill_function SIGUSR1
		;;
	resume)
		is_running 'resume'
		log_function resume
		kill_function SIGCONT
		;;
	stop)
		is_running 'stop'
		kill_function SIGTERM
		;;
	query)
		[[ "$1" == all ]] && { cat $log_file; exit 0; }
		[[ -e $pid_file ]] && { 
			kill_function SIGUSR2; 
			sleep 0.125s; 
			echo "$(date) :: remaining[$(cat ~/.log/psleep)]" >>$log_file; 
			sleep 0.125s; 
		}
		[[ -z "$1" ]] && { tail -n1 $log_file; exit 0; }
		;;
	add)
		echo 'format: [name] [[hh:]mm:]ss'
		cat | tee $db_file >&3
		;;
	next)
		cat $db_file | grep -q '.' || { 
			rm $log_file; 
			echo >&3;
			date >&3;
			task add; 
		} 
		read name duration <$db_file
		tail -n+2 $db_file >/tmp/tasks_tmp
		mv /tmp/tasks_tmp $db_file
		task start $name $duration && (sleep 0.25s; task query)
		;;
	edit)
		vim $db_file
		;;
	clear)
		echo >$db_file
		;;
	''|list)
		task query item
		task query all
		cat $db_file
		;;
	help)
		usage help
		;;
	*)
		usage
		;;
esac
#TODO: echo $name into $pid_file, and instead of cat-ing from it, use read PID name <$pid_file
exec 3>&- 
