---
title: task
author: bodoki-halmen zsolt
date: 2020-2021
---
## `task`
### a helper program (written in bash + python) for people with time management problems

#### about `task`

`task` is a simple command line interface program, which helps with managing your time.

you can type in your planned activities at the beginning of the day, 
	with the amount of time you would like to spend on each activity.
	
invoking `task next` will give you an input prompt,
	and after hitting the EOF character (`ctrl-D` on an empty line, on most linux distributions),
	it will automatically start a counter for the first task.

`task query`, `task query all`, or simply `task` will give you insight into the time elapsed,
	or into the log of the day.
	you can pause and resume the counter (`task pause` or `task resume` respectively),
	anytime you must take a break in your activity, or force stop the counter (`task stop`) -
	should you finish before planned, or something came up which changed the schedule completely.
	
after the counter completes, the computers screen gets locked, and a message is displayed after unlocking.
(this feature depends on `slock` and `dmenu`).

#### in depth features
in this repository, you will find a screen cast, introducing the features of task. (which can also be viewed as a tutorial)

#### installation
clone/download the repository, and add `task` and `psleep` to your list of executables:
	1. copy/move them to /bin
	1. copy/move them to /usr/bin
	1. copy/move them to a directory which is already included in your $PATH
	1. add the path to the containing directory to your $PATH (a) 
	1. leave them where they are:
		- run `task` from this directory (a)
		- create a link to task in one of the directories from points 1 through 3. you might need to link `psleep` as well.

(a) (make sure that the partition has execution priviledges)

#### portability and dependencies
the program is written in bash and pure python 3, so both are dependencies.

any other dependency and help on porting/modifying the behaviour can be found as comments in the source code of `task`
